from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView,CreateAPIView
from . models import * 
from . serializer import *


class GetHybel(ListAPIView):
    queryset=Hybel.objects.all()
    serializer_class = HybelSerializer

class AddHybel(CreateAPIView):
    queryset=Hybel.objects.all()
    serializer_class = HybelSerializer

