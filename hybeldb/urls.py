from django.urls import path
from .views import *

from . import views

app_name ="hybel-app"
urlpatterns = [
    path('create/', views.AddHybel.as_view()),
    path('view/', views.GetHybel.as_view()),
]


